const axios = require("axios");
const moment = require("moment-timezone");

const { getLeagueId, FOOTBALL_API_URL, TIMEZONE } = require("./../../config/constant");

const getFootballMatchesInfo = async matchCommand => {
  const leagueId = getLeagueId(matchCommand);
  const season = leagueId === 2 ? 2022 : 2023; // if league id is equal to champions league, use season 2021

  const headers = {
    "x-apisports-key": process.env.API_SPORTS_KEY
  };
  const params = {
    league: leagueId,
    season,
    date: moment()
      .tz(TIMEZONE)
      .format("YYYY-MM-DD"),
    timezone: TIMEZONE
  };

  const result = await axios.get(`${FOOTBALL_API_URL}/fixtures`, {
    headers,
    params
  });

  if (result.data && result.data.response.length > 0) {
    let responseMatches = `Los partidos de hoy en ${matchCommand} son: \n`;
    result.data.response.map(res => {
      const matchHour = moment(res.fixture.date)
        .tz(TIMEZONE)
        .locale("es-mx")
        .calendar();
      if (res.fixture.status.elapsed === 90) {
        responseMatches +=
          "\n" +
          res.teams.home.name +
          " v/s " +
          res.teams.away.name +
          " " +
          matchHour +
          "\n" +
          res.goals.home +
          " - " +
          res.goals.away +
          "\n";
      }
      if (res.fixture.status.elapsed < 90 && res.fixture.status.elapsed > 0) {
        responseMatches +=
          "\n" +
          res.teams.home.name +
          " v/s " +
          res.teams.away.name +
          " " +
          matchHour +
          "\n" +
          " minuto: " +
          res.fixture.status.elapsed +
          "\n" +
          res.goals.home +
          " - " +
          res.goals.away +
          "\n";
      }
      if (res.fixture.status.elapsed === null) {
        responseMatches +=
          "\n" + res.teams.home.name + " v/s " + res.teams.away.name + " " + matchHour;
      }
    });
    return responseMatches;
  } else {
    return `No hay partidos hoy en ${matchCommand} ):`;
  }
};

module.exports = {
  getFootballMatchesInfo
};
