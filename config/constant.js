const COMMAND_CHILEAN_PREMIERE_LEAGUE = "cplmatches";
const COMMAND_WORLD_CUP_QUALIFICATION = "eliminatorias_qatar";
const COMMAND_SHEMPIENS = "shempiens";
const COMMAND_LIBERTADORES = "libertadores";

const CHILEAN_PREMIERE_LEAGUE_ID = 265;
const QUALIFICATION_WORLD_CUP_SOUTHAMERICA_ID = 34;
const SHEMPIENS_ID = 2;
const LIBERTADORES_ID = 13;

const LEAGUE_IDS = {
  [COMMAND_CHILEAN_PREMIERE_LEAGUE]: CHILEAN_PREMIERE_LEAGUE_ID,
  [COMMAND_WORLD_CUP_QUALIFICATION]: QUALIFICATION_WORLD_CUP_SOUTHAMERICA_ID,
  [COMMAND_SHEMPIENS]: SHEMPIENS_ID,
  [COMMAND_LIBERTADORES]: LIBERTADORES_ID
};

const getLeagueId = value => {
  if (Object.keys(LEAGUE_IDS).includes(value)) {
    return LEAGUE_IDS[value];
  } else {
    return value;
  }
};

module.exports = {
  SHOULD_I_DEPLOY_API_URL: "https://shouldideploy.today/api?tz=UTC",
  ADVICE_API_URL: "https://api.adviceslip.com/advice",
  HOLLIDAYS_API_URL: "https://apis.digital.gob.cl/fl/feriados",
  FOOTBALL_API_URL: "https://v3.football.api-sports.io",
  TIMEZONE: "America/Santiago",
  getLeagueId
};
