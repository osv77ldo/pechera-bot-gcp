# Pechera Dev Cell Bot

This project is a POC of telegram bot by pechera dev cell

Related Docs: [API Telegram](https://core.telegram.org/)


## External Npm Packages

[Telegraf](telegraf.js.org)


## Installation 

### Install dependencies
npm install


### Start the project
npm run start


