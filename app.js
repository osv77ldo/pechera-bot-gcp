require("dotenv").config();

const Telegraf = require("telegraf");

const { BOT_TOKEN, BOT_URL, RESPONSE_GRETTINGS } = process.env;

const bot = new Telegraf(BOT_TOKEN, { username: "pechera_bot" });
bot.telegram.setWebhook(BOT_URL);

const {
  aboutCommand,
  shouldIDeployCommand,
  adviceCommand,
  getHollidays,
  getFootballMatchInfo,
  helpCommand
} = require("./helpers/commands");

const {
  dfcHears,
  donzunaHears,
  rodrigoHears,
  pepiloHears,
  ernestoHears,
  tomyHears,
  cristianoHears,
  osvaldoHears,
  regretHears,
  juniorHears,
  calorHears
} = require("./helpers/hears");

bot.start(ctx => ctx.reply(RESPONSE_GRETTINGS, { reply_to_message_id: ctx.message.message_id }));

// Telegram command handler
aboutCommand(bot);
shouldIDeployCommand(bot);
adviceCommand(bot);
helpCommand(bot);
getHollidays(bot);
getFootballMatchInfo(bot);

// Telegram hears handler
dfcHears(bot);
donzunaHears(bot);
rodrigoHears(bot);
pepiloHears(bot);
ernestoHears(bot);
tomyHears(bot);
cristianoHears(bot);
osvaldoHears(bot);
regretHears(bot);
juniorHears(bot);
calorHears(bot);

exports.botFunction = async (req, res) => {
  try {
    await bot.handleUpdate(req.body);
  } finally {
    res.status(200).end();
  }
};
