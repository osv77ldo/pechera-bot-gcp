const moment = require("moment-timezone");
const axios = require("axios");

moment.locale("es");

const { getFootballMatchesInfo } = require("../infrastructure/http/footballApi");

const {
  ADVICE_API_URL,
  SHOULD_I_DEPLOY_API_URL,
  HOLLIDAYS_API_URL
} = require("../config/constant");

const { RESPONSE_HOLLIDAYS, RESPONSE_HELP } = process.env;

const aboutCommand = bot => {
  bot.command("about", ctx => {
    const about = "**Pechera Dev Cell Bot 😈**";
    ctx.replyWithMarkdown(about);
  });
};

const shouldIDeployCommand = bot => {
  bot.command("should_i_deploy", async ctx => {
    const result = await axios.get(SHOULD_I_DEPLOY_API_URL);
    if (result.data) {
      ctx.reply(result.data.message);
    }
  });
};

const adviceCommand = bot => {
  bot.command("advice", async ctx => {
    const result = await axios.get(ADVICE_API_URL);
    if (result.data) {
      ctx.reply(result.data.slip.advice);
    }
  });
};

const helpCommand = bot => {
  bot.command("help", async ctx => {
    ctx.reply(RESPONSE_HELP);
  });
};

const getHollidays = bot => {
  bot.command("feriados", async ctx => {
    const month = moment().month() + 1;
    const hollidays = await axios.get(HOLLIDAYS_API_URL + "/" + moment().year() + "/" + month);
    let responseHollidays = "";
    if (hollidays.data.length > 0) {
      hollidays.data.forEach(holliday => {
        if (moment(holliday.fecha) >= moment()) {
          responseHollidays +=
            "\n" +
            moment(holliday.fecha).format("dddd, MMMM Do YYYY") +
            " con motivo de " +
            holliday.nombre;
        }
      });
      if (responseHollidays.length > 0) {
        ctx.reply(RESPONSE_HOLLIDAYS + responseHollidays);
      } else {
        ctx.reply("Los feriados de este mes ya pasaron");
      }
    } else {
      ctx.reply("este mes no tiene feriados csmmm");
    }
  });
};

const getFootballMatchInfo = bot => {
  bot.command("futbol", async ctx => {
    bot.telegram.sendMessage(ctx.chat.id, "Indicame la liga por la que quieres consultar ⚽", {
      reply_markup: {
        inline_keyboard: [
          [
            {
              text: "Chilean Premier League 🇨🇱",
              callback_data: "chilean_premier_league"
            }
          ],
          [
            {
              text: "Eliminatorias Qatar 2022 🇶🇦 ",
              callback_data: "eliminatorias_qatar"
            }
          ],
          [{ text: "Champions League ⚽", callback_data: "shempiens" }],
          [{ text: "Copa Libertadores 🏆", callback_data: "libertadores" }]
        ]
      }
    });
  });
  bot.action("chilean_premier_league", async ctx => {
    ctx.deleteMessage();
    ctx.reply(await getFootballMatchesInfo("cplmatches"));
  });
  bot.action("eliminatorias_qatar", async ctx => {
    ctx.deleteMessage();
    ctx.reply(await getFootballMatchesInfo("eliminatorias_qatar"));
  });
  bot.action("shempiens", async ctx => {
    ctx.deleteMessage();
    ctx.reply(await getFootballMatchesInfo("shempiens"));
  });
  bot.action("libertadores", async ctx => {
    ctx.deleteMessage();
    ctx.reply(await getFootballMatchesInfo("libertadores"));
  });
};

module.exports = {
  aboutCommand,
  shouldIDeployCommand,
  adviceCommand,
  helpCommand,
  getHollidays,
  getFootballMatchInfo
};
