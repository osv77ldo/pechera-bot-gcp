const {
  RESPONSE_DONZUNA,
  RESPONSE_PEPILLO,
  RESPONSE_DFC,
  RESPONSE_RODRIGO,
  RESPONSE_ERNES3,
  RESPONSE_OSVALDO,
  RESPONSE_TOMY,
  RESPONSE_FORGIVENESS,
  RESPONSE_GUILTY,
  RESPONSE_JUNIOR
} = process.env;

const dfcHears = bot => {
  bot.hears([/DFC/, /dfc/, /Dfc/], ctx =>
    ctx.reply(RESPONSE_DFC, { reply_to_message_id: ctx.message.message_id })
  );
};

const donzunaHears = bot => {
  bot.hears([/Donzuna/, /donzuna/], ctx =>
    ctx.reply(RESPONSE_DONZUNA, { reply_to_message_id: ctx.message.message_id })
  );
};

const rodrigoHears = bot => {
  bot.hears([/Rodrigo/, /rodrigo/], ctx =>
    ctx.reply(RESPONSE_RODRIGO, { reply_to_message_id: ctx.message.message_id })
  );
};

const pepiloHears = bot => {
  bot.hears([/Pepillo/, /pepillo/], ctx =>
    ctx.reply(RESPONSE_PEPILLO, { reply_to_message_id: ctx.message.message_id })
  );
};
const ernestoHears = bot => {
  bot.hears([/Ernes3/, /ernes3/], ctx =>
    ctx.reply(RESPONSE_ERNES3, { reply_to_message_id: ctx.message.message_id })
  );
};

const osvaldoHears = bot => {
  bot.hears([/Osvaldo/, /osval3/, /osvaldo/], ctx =>
    ctx.reply(RESPONSE_OSVALDO, { reply_to_message_id: ctx.message.message_id })
  );
};

const tomyHears = bot => {
  bot.hears([/Tomy/, /tomy/], ctx =>
    ctx.reply(RESPONSE_TOMY, { reply_to_message_id: ctx.message.message_id })
  );
};

const juniorHears = bot => {
  bot.hears([/junior/, /jr/], ctx =>
    ctx.reply(RESPONSE_JUNIOR, { reply_to_message_id: ctx.message.message_id })
  );
};

const cristianoHears = bot => {
  bot.hears([/CR7/, /cr7/, /Cr7/], ctx =>
    ctx.replyWithVoice("https://storage.googleapis.com/pechera-bot-media-files/siu.mp3", {
      reply_to_message_id: ctx.message.message_id
    })
  );
};

const calorHears = bot => {
  bot.hears([/Calor/, /calor/], ctx =>
    ctx.replyWithVoice("https://storage.googleapis.com/pechera-bot-media-files/calor.mp3", {
      reply_to_message_id: ctx.message.message_id
    })
  );
};

const regretHears = bot => {
  bot.hears([/perdon al admin/, /perdón al admin/, /Perdón al admin/], ctx => {
    if (new Date().getSeconds() % 2 == 0) {
      ctx.reply(RESPONSE_FORGIVENESS, { reply_to_message_id: ctx.message.message_id });
    } else {
      ctx.reply(RESPONSE_GUILTY, { reply_to_message_id: ctx.message.message_id });
    }
  });
};

module.exports = {
  dfcHears,
  donzunaHears,
  rodrigoHears,
  pepiloHears,
  ernestoHears,
  tomyHears,
  cristianoHears,
  calorHears,
  osvaldoHears,
  regretHears,
  juniorHears
};
